package br.com.fiap.recyclerviewcomjava;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class JogoAdapter extends RecyclerView.Adapter<JogoViewHolder>{

    private Context context;
    private ArrayList<Jogo> itens;

    public JogoAdapter(Context context, ArrayList<Jogo> itens) {
        this.context = context;
        this.itens = itens;
    }

    @NonNull
    @Override
    public JogoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {

        View  tela = LayoutInflater.from(context).inflate(R.layout.jogo_linha, viewGroup, false);
        JogoViewHolder viewHolder = new JogoViewHolder(tela);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull JogoViewHolder jogoViewHolder, int position) {

        Jogo jogo = itens.get(position);
        jogoViewHolder.nome.setText(jogo.getNome());
        jogoViewHolder.plataforma.setText(jogo.getPlataforma());
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }
}
