package br.com.fiap.recyclerviewcomjava;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class JogoViewHolder extends RecyclerView.ViewHolder {

    TextView nome, plataforma;

    public JogoViewHolder(@NonNull View itemView) {
        super(itemView);
        nome = itemView.findViewById(R.id.nome);
        plataforma = itemView.findViewById(R.id.plataforma);

    }


}