package br.com.fiap.recyclerviewcomjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private JogoAdapter adapter;
    private ArrayList<Jogo> itens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recycler = findViewById(R.id.recycler);
        itens = new ArrayList<Jogo>();
        itens.add(new Jogo("Halo", "Xbox"));
        itens.add(new Jogo("God of War", "Playstation"));
        itens.add(new Jogo("Pokemon", "Nintendo"));

        adapter = new JogoAdapter(MainActivity.this, itens);
        LinearLayoutManager linearLayout = new LinearLayoutManager(
                getApplicationContext(),
                LinearLayoutManager.VERTICAL,
                false
        );

        recycler.setAdapter(adapter);
        recycler.setLayoutManager(linearLayout);
    }
}
